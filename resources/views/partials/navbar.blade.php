<nav id="navbar">
    <div class="container">
        <div class="nav-wrapper">
            <a href="{{url('/')}}" class="brand-logo">{{config('app.name')}}</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="sass.html">Política de privacidade</a></li>
                @if (isset($navLinks))
                    {!! $navLinks !!}
                @endif
            </ul>
        </div>
    </div>
</nav>

<ul class="sidenav" id="mobile-demo">
    <li><a href="sass.html">Política de privacidade</a></li>
    @if (isset($navLinks))
        {!! $navLinks !!}
    @endif
</ul>