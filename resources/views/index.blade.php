@extends('layout.app')

@section('title', 'Crie seu sorteio agora!')
@section('body-class', 'index-page')

@section('content')

    @component('components.header')
        <div class="container content-header">
            <div class="row">
                <div class="col s12 m5">
                    <h1>Faça agora um sorteio no Instagram!</h1>
                    <h3>Está querendo sortear algo para seus seguidores de maneira rápida, transparente e confiável?</h3>
                    <a id="generate-sort" class="btn btn-large white blue-text text-darken-2 modal-trigger" href="#user-data-modal">Gerar sorteio</a>
                </div>
                <div class="col s12 m7">
                    <img src="{{asset('assets/img/illustration.png')}}">
                </div>
            </div>
        </div>
    @endcomponent

    @include('partials.login_modal')

@endsection
