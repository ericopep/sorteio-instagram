<header id="header">
    @include('partials.navbar', (isset($links) ? ['navLinks' => $links] : []))
    {{$slot}}
</header>