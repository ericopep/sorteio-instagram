@if (isset($beforeOut)) {{$beforeOut}} @endif
<div class="card card-media" data-code="{{$media->shortCode}}">
    @if (isset($before)) {{$before}} @endif
    <div class="card-image">
        <div class="img-box">
            <img src="{{$media->imageHighResolutionUrl}}">
        </div>
    </div>
    <div class="card-content">
        <div class="row">
            <div class="col s6">
                <i class="tiny material-icons">comment</i>
                <span class="comments-count">{{$media->commentsCountFormated}}</span>
            </div>
            <div class="col s6">
                <i class="tiny material-icons">favorite</i>
                <span class="likes-count">{{$media->likesCountFormated}}</span>
            </div>
        </div>
    </div>
    @if (isset($after)) {{$after}} @endif
</div>
