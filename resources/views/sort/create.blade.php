@extends('layout.app')

@section('title', 'Selecione a imagem!')
@section('body-class', 'full-page')

@section('content')

    @component('components.header')
        @slot('links')
            <li><a href="#user-data-modal" class="modal-trigger">Novo sorteio</a></li>
        @endslot
    @endcomponent

    <div class="container">
        <h1>Escolha uma postagem</h1>
        <form method="post" action="" id="form-search-media">
            <div class="row">
                <div class="col s12 m9">
                    <div class="input-field input-white">
                        <input id="url-input" type="url" class="validate" name="email" required>
                        <label for="url-input">Link da postagem no Instagram</label>
                    </div>
                </div>
                <div class="col s12 m3">
                    <button class="btn white blue-text text-darken-2 btn-large">Pesquisar</button>
                </div>
            </div>
        </form>

        <h4>Ou escolha uma postagem de <strong>{{'@'.session('user')->getUsername()}}</strong></h4>
        <div class="row media-list">
            @foreach ($medias as $media)
                <div class="col s6 m3">
                    @component('components.card_media', ['media' => $media])
                    @endcomponent
                </div>
            @endforeach
        </div>
    </div>

    <div id="search-result-modal" class="modal">
        <div class="modal-content">
            @component('components.card_media', ['media' => new \App\Backend\Instagram\Model\Media])
                @slot('before')
                    <div class="chip">
                        <img src="">
                        <span></span>
                    </div>
                @endslot
                @slot('after')
                    <div class="card-action">
                        <a href="javascript:;" id="select-search-modal">Selecionar</a>
                        <a href="javascript:;" id="cancel-search-modal">Cancelar</a>
                    </div>
                @endslot
            @endcomponent
        </div>
    </div>

    @include('partials.spinner')
    @include('partials.login_modal')

    <form method="post" action="{{route('sort.store')}}" id="form-code">
        {{csrf_field()}}
        {{method_field('put')}}
        <input type="hidden" name="code" id="code-input">
    </form>

@endsection

@section('scripts')

    <script>
        var currentUser = { username: '{{session('user')->getUsername()}}', profile_pic_url:  '{{session('user')->getProfilePicUrl()}}' },
            hasMediaEnd = {{!$hasNext ? 'true' : 'false'}},
            mediaMaxId = {!! $hasNext ? '"' . $maxId . '"' : 'null' !!};
    </script>
    <script src="{{ asset('assets/js/sort.js') }}"></script>

@endsection
