@extends('layout.app')

@section('title', 'Baixando os comentários')
@section('body-class', 'full-page')

@section('content')

    @component('components.header')
        @slot('links')
            <li><a href="#user-data-modal" class="modal-trigger">Novo sorteio</a></li>
        @endslot
    @endcomponent

    <div class="container download-page">
        <h1>Baixar comentários</h1>

        <div class="row">
            <div class="col s12 m4 offset-m4">
                <p>
                    <label>
                        <input type="checkbox" id="notify" class="filled-in checkbox-white" />
                        <span>Notificar quando finalizar</span>
                    </label>
                </p>
                <span class="percent">{{$percent}}%</span>
                @component('components.card_media', ['media' => $media])
                @endcomponent
            </div>
        </div>
    </div>

    @include('partials.login_modal')

@endsection

@section('scripts')

    <script>
        var playerId = null;

        function getSubscriptionState() {
            return Promise.all([
                OneSignal.isPushNotificationsEnabled(),
                OneSignal.isOptedOut()
            ]).then(function(result) {
                var isPushEnabled = result[0];
                var isOptedOut = result[1];

                return {
                    isPushEnabled: isPushEnabled,
                    isOptedOut: isOptedOut
                };
            });
        }

        var updateNotifyInServer = function(playerIdParam) {
            $.ajax({
                type: 'post',
                url: '{{route('sort.updateNotify', [$sort->id])}}',
                data: { playerId: playerIdParam },
                error: function(request) {
                    var jsonResponse = $.parseJSON(request.response);
                    toastError(jsonResponse.message);
                }
            })
        };

        var updatePlayerIdInServer = function() {
            if ($('#notify').is(':checked') && playerId) {
                updateNotifyInServer(playerId);
            }
        };

        function onManageWebPushSubscriptionButtonClicked() {
            getSubscriptionState().then(function(state) {
                if (state.isPushEnabled) {
                    /* Subscribed, opt them out */
                    OneSignal.setSubscription(true);
                } else {
                    if (state.isOptedOut) {
                        /* Opted out, opt them back in */
                        OneSignal.setSubscription(true);
                    } else {
                        /* Unsubscribed, subscribe them */
                        OneSignal.registerForPushNotifications();
                    }
                }
                getDeviceId();
            });
        }

        var getStatus = function() {
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '{{route('sort.status', [$sort->id])}}',
                success: function(data) {
                    var percent;

                    if (data.downloaded) {
                        percent = 100;
                        $('.percent').text(percent + '%');
                        location.reload();
                    } else {
                        percent = data.percent;
                        $('.percent').text(percent + '%');
                        setTimeout(getStatus, 1500);
                    }
                }
            });
        };

        var getDeviceId = function() {
            if (playerId == null) {
                OneSignal.getUserId(function (userId) {
                    playerId = userId;
                    updatePlayerIdInServer();
                });
            } else {
                updatePlayerIdInServer();
            }
        };

        $('#notify').click(function() {
            if ($(this).is(':checked')) {
                onManageWebPushSubscriptionButtonClicked();
            } else {
                updateNotifyInServer(null);
            }
        });

        OneSignal.push(function() {
            if (!OneSignal.isPushNotificationsSupported()) {
                return;
            }

            OneSignal.on("subscriptionChange", function(isSubscribed) {
                getDeviceId();
            });
        });

        getStatus();
    </script>

@endsection

@section('stylesheets')

    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js"></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
            OneSignal.init({
                appId: "5e5a822c-b522-4c6b-8bf8-09de56d88df9",
                autoRegister: false,
                notifyButton: {
                    enable: false
                },
                welcomeNotification: {
                    disable: true
                }
            });
        });
    </script>

@endsection