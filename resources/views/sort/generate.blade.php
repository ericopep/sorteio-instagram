@extends('layout.app')

@section('title', 'Sortear ganhador')
@section('body-class', 'full-page')

@section('content')

    @component('components.header')
        @slot('links')
            <li><a href="#user-data-modal" class="modal-trigger">Novo sorteio</a></li>
        @endslot
    @endcomponent

    <div class="container download-page">
        <h1>Sortear ganhador</h1>

        <div class="row">
            <div class="col s12 m4">
                @component('components.card_media', ['media' => $media])
                    @slot('beforeOut')
                        <div class="chip">
                            <img src="{{$media->user->profilePicUrl}}">
                            {{$media->user->username}}
                        </div>
                    @endslot
                @endcomponent
            </div>
            <div class="col s12 m4">
                <form method="post" action="javascript:;" id="options-form">
                    <div class="input-field input-white">
                        <input id="num_winners" type="text" name="num_winners" value="1">
                        <label for="num_winners">Número de ganhadores</label>
                    </div>
                    <p>
                        <label>
                            <input type="checkbox" id="unique_winners" name="unique_winners" class="filled-in checkbox-white" checked />
                            <span>Não repetir ganhador</span>
                        </label>
                    </p>
                    <strong class="white-text">Filtros:</strong>
                    <div class="input-field  input-white">
                        <input id="keyword_filter" type="text" name="keyword_filter" value="">
                        <label for="keyword_filter">Apenas comentários com a palavra chave:</label>
                    </div>
                    <p>
                        <label>
                            <input type="checkbox" id="unique_user_comment" class="filled-in checkbox-white" />
                            <span>Apenas um comentário por pessoa</span>
                        </label>
                    </p>
                    <p>
                        <label>
                            <input type="checkbox" id="unique_comments" class="filled-in checkbox-white" />
                            <span>Remover comentários repetidos</span>
                        </label>
                    </p>
                    <p>
                        <button class="btn btn-large white blue-text text-darken-2" id="sort-btn">Sortear</button>
                    </p>
                </form>
            </div>
            <div class="col s12 m4">
                <h4>Resultado:</h4>
                <div class="result">
                    <div class="card-panel yellow accent-3 black-text">
                        Clique em <strong>SORTEAR</strong> para ver os ganhadores.
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.spinner')
    @include('partials.login_modal')

@endsection

@section('scripts')

    <script>
        var loading = false;
        $('#options-form').submit(function() {
            if (loading)
                return false;

            loading = true;
            var numWinners = $('#num_winners');
            var uniqueWinners = $('#unique_winners');
            var keywordFilter = $('#keyword_filter');
            var uniqueUserComment = $('#unique_user_comment');
            var uniqueComments = $('#unique_comments');

            $('#sort-btn').prop('disabled', true);
            $('.result').html( $('#spinner-loading').clone().wrap("<div />").parent().html() );

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '{{ route('sort.generate', ['id' => $sort->id]) }}',
                data: {
                    numWinners: numWinners.val(),
                    uniqueWinners: uniqueWinners.prop('checked') ? 1: null,
                    keywordFilter: keywordFilter.val(),
                    uniqueUserComment: uniqueUserComment.prop('checked') ? 1: null,
                    uniqueComments: uniqueComments.prop('checked') ? 1: null
                },
                error: function(request) {
                    var jsonResponse = $.parseJSON(request.response);
                    toastError(jsonResponse.message);
                },
                success: function(response) {
                    if ($.isEmptyObject(response.data)) {
                        $('.result').html('<div class="card-panel yellow accent-3 black-text">Nenhum sorteado! Verifique as configurações.</div>');
                        return false;
                    }

                    var template = '<div class="card-panel white black-text winner">' +
                        '<strong>#{rowid} - {user}</strong>' +
                        '{comment_text}' +
                        '</div>';

                    var html = '';
                    $.each(response.data, function(i, item) {
                        html += template.replace('{rowid}', item.rowid).
                        replace('{user}', item.user).
                        replace('{comment_text}', item.comment_text);
                    });

                    $('.result').html(html);
                },
                complete: function() {
                    loading = false;
                    $('#sort-btn').prop('disabled', false);
                }
            });

            return false;
        });
    </script>

@endsection
