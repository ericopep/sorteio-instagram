<header id="header">
    <?php echo $__env->make('partials.navbar', (isset($links) ? ['navLinks' => $links] : []), \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo e($slot); ?>

</header>