<?php $__env->startSection('title', 'Baixando os comentários'); ?>
<?php $__env->startSection('body-class', 'full-page'); ?>

<?php $__env->startSection('content'); ?>

    <?php $__env->startComponent('components.header'); ?>
        <?php $__env->slot('links'); ?>
            <li><a href="#user-data-modal" class="modal-trigger">Novo sorteio</a></li>
        <?php $__env->endSlot(); ?>
    <?php echo $__env->renderComponent(); ?>

    <div class="container download-page">
        <h1>Baixar comentários</h1>

        <div class="row">
            <div class="col s12 m4 offset-m4">
                <p>
                    <label>
                        <input type="checkbox" id="notify" class="filled-in checkbox-white" />
                        <span>Notificar quando finalizar</span>
                    </label>
                </p>
                <span class="percent"><?php echo e($percent); ?>%</span>
                <?php $__env->startComponent('components.card_media', ['media' => $media]); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
        </div>
    </div>

    <?php echo $__env->make('partials.login_modal', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

    <script>
        var currentUser = { username: '<?php echo e(session('user')->getUsername()); ?>', profile_pic_url:  '<?php echo e(session('user')->getProfilePicUrl()); ?>' };

        var playerId = null;

        function getSubscriptionState() {
            return Promise.all([
                OneSignal.isPushNotificationsEnabled(),
                OneSignal.isOptedOut()
            ]).then(function(result) {
                var isPushEnabled = result[0];
                var isOptedOut = result[1];

                return {
                    isPushEnabled: isPushEnabled,
                    isOptedOut: isOptedOut
                };
            });
        }

        var updateNotifyInServer = function(playerIdParam) {
            $.ajax({
                type: 'post',
                url: '<?php echo e(route('sort.updateNotify', [$sort->id])); ?>',
                data: { playerId: playerIdParam },
                error: function(request) {
                    var jsonResponse = $.parseJSON(request.response);
                    toastError(jsonResponse.message);
                }
            })
        };

        var updatePlayerIdInServer = function() {
            if ($('#notify').is(':checked') && playerId) {
                updateNotifyInServer(playerId);
            }
        };

        function onManageWebPushSubscriptionButtonClicked() {
            getSubscriptionState().then(function(state) {
                if (state.isPushEnabled) {
                    /* Subscribed, opt them out */
                    OneSignal.setSubscription(true);
                } else {
                    if (state.isOptedOut) {
                        /* Opted out, opt them back in */
                        OneSignal.setSubscription(true);
                    } else {
                        /* Unsubscribed, subscribe them */
                        OneSignal.registerForPushNotifications();
                    }
                }
                getDeviceId();
            });
        }

        var getStatus = function() {
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '<?php echo e(route('sort.status', [$sort->id])); ?>',
                success: function(data) {
                    var percent;

                    if (data.downloaded) {
                        percent = 100;
                        $('.percent').text(percent + '%');
                        location.reload();
                    } else {
                        percent = data.percent;
                        $('.percent').text(percent + '%');
                        setTimeout(getStatus, 1500);
                    }
                }
            });
        };

        var getDeviceId = function() {
            if (playerId == null) {
                OneSignal.getUserId(function (userId) {
                    playerId = userId;
                    updatePlayerIdInServer();
                });
            }
        };

        $('#notify').click(function() {
            if ($(this).is(':checked')) {
                onManageWebPushSubscriptionButtonClicked();
            } else {
                updateNotifyInServer(null);
            }
        });

        OneSignal.push(function() {
            if (!OneSignal.isPushNotificationsSupported()) {
                return;
            }

            OneSignal.on("subscriptionChange", function(isSubscribed) {
                getDeviceId();
            });
        });

        getStatus();
    </script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('stylesheets'); ?>

    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js"></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
            OneSignal.init({
                appId: "5e5a822c-b522-4c6b-8bf8-09de56d88df9",
                autoRegister: false,
                notifyButton: {
                    enable: false
                }
            });
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>