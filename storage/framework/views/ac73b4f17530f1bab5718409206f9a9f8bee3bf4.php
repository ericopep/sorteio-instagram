<?php $__env->startSection('title', 'Crie seu sorteio agora!'); ?>
<?php $__env->startSection('body-class', 'index-page'); ?>

<?php $__env->startSection('content'); ?>

    <?php $__env->startComponent('components.header'); ?>
        <div class="container content-header">
            <div class="row">
                <div class="col s12 m5">
                    <h1>Faça agora um sorteio no Instagram!</h1>
                    <h3>Está querendo sortear algo para seus seguidores de maneira rápida, transparente e confiável?</h3>
                    <a id="generate-sort" class="btn btn-large white blue-text text-darken-2 modal-trigger" href="#user-data-modal">Gerar sorteio</a>
                </div>
                <div class="col s12 m7">
                    <img src="<?php echo e(asset('assets/img/illustration.png')); ?>">
                </div>
            </div>
        </div>
    <?php echo $__env->renderComponent(); ?>

    <?php echo $__env->make('partials.login_modal', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>