<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo $__env->yieldContent('title'); ?> - <?php echo e(config('app.name')); ?></title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/css/app.css')); ?>" />
    <?php echo $__env->yieldContent('stylesheets'); ?>
</head>
<body class="<?php echo $__env->yieldContent('body-class'); ?>">

    <?php echo $__env->yieldContent('content'); ?>

    <script>var base_url = '<?php echo e(url('/')); ?>';</script>
    <script src="//zeptojs.com/zepto.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="<?php echo e(asset('assets/js/app.js')); ?>"></script>
    <?php echo $__env->yieldContent('scripts'); ?>
    <?php if($errors->any()): ?>
        <script>
            toastError('<?php echo e($errors->first('message')); ?>');
        </script>
    <?php endif; ?>

</body>
</html>
