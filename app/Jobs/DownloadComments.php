<?php

namespace App\Jobs;

use App\Backend\Database\SortRequestDatabase;
use App\Backend\Instagram\Instagram;
use App\Helpers\SqliteDatabaseHelper;
use App\Models\Sort;
use App\Notifications\GetCommentsCompleted;
use App\Notifications\GetCommentsError;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class DownloadComments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private static $dbHelper;
    private $code;
    private $sortId;
    private $path;
    private $errorCount = 0;
    private $errorMax = 10;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sortId, $code)
    {
        $this->sortId = $sortId;
        $this->code = $code;
        $this->path = storage_path('app/db/' . $code . '.sqlite');
    }

    private function notify($notification)
    {
        $sort = Sort::find($this->sortId);
        $sort->notify($notification);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        self::$dbHelper = new SqliteDatabaseHelper($this->code, true);

        // create database
        $this->createDatabase(static::$dbHelper->db);

        // get comments
        $hasNext = true;
        $maxId = null;

        while ($hasNext) {
            try {
                $commentsResponse = Instagram::getMediaComments($this->code, $maxId);
                $comments = $commentsResponse['comments'];

                static::$dbHelper->db->exec('BEGIN;');

                $count = 0;
                foreach ($comments as $comment) {

                    $stmt = self::$dbHelper->db->prepare('INSERT INTO comments(comment_id, comment_text, user, date) VALUES (:id, :text, :username, :date)');
                    $stmt->bindValue(':id', $comment->getId());
                    $stmt->bindValue(':username', $comment->getOwner()->getUsername());
                    $stmt->bindValue(':text', $comment->getText());
                    $stmt->bindValue(':date', $comment->getCreatedAt(), SQLITE3_INTEGER);
                    $stmt->execute();
                    $count++;

                }

                static::$dbHelper->db->exec('COMMIT;');

                Sort::where('id', $this->sortId)
                    ->update(['comments_downloaded' => DB::raw('comments_downloaded + ' . $count)]);

                $hasNext = $commentsResponse['hasNext'];
                $maxId = $commentsResponse['maxId'];
                $this->errorCount = 0;
                usleep(3500000);
            } catch (Exception $e) {
                $this->errorCount++;

                if ($this->errorCount >= $this->errorMax) {
                    $this->notify(new GetCommentsError);
                    break;
                }
            }
        }

        // update success
        // get sort
        $sort = Sort::find($this->sortId);
        $sort->downloaded = 1;
        $sort->save();
        $this->notify(new GetCommentsCompleted);
    }

    /**
     * Create table
     */
    public function createDatabase($db)
    {
        $sql = 'CREATE TABLE comments (
                 comment_id text PRIMARY KEY,
                 comment_text text NOT NULL,
                 user text NOT NULL,
                 date int NOT NULL
                );';

        $db->exec($sql);
    }
}
