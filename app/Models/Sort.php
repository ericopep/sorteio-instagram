<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Sort extends Model
{
    use Notifiable;

    protected $casts = [
        'media_data' => 'array',
        'notify_data' => 'array'
    ];

    public function routeNotificationForOneSignal()
    {
        if (isset($this->notify_data['id']))
            return $this->notify_data['id'];

        return null;
    }
}
