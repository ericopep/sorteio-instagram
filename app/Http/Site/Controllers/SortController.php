<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 04/10/18
 * Time: 10:56
 */

namespace App\Http\Site\Controllers;

use App\Backend\Instagram\Instagram;
use App\Backend\Instagram\Model\Media;
use App\Helpers\SqliteDatabaseHelper;
use App\Jobs\DownloadComments;
use App\Models\Sort;
use App\Models\User;
use App\Notifications\GetCommentsCompleted;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use PDO;

class SortController extends BaseController
{

    /**
     * @param Request $request
     */
    public function start(Request $request)
    {
        try {
            $username = $request->input('username');
            $email = $request->input('email');

            // validate request
            $request->validate([
                'username' => 'required',
                'email' => 'required|email'
            ]);

            // get data from instagram
            $instagram = new Instagram($username);
            $profilePicture = $instagram->getUserProfilePicture();
            $medias = $instagram->getMedias();

            // save user in database
            $user = new User;
            $user->username = $username;
            $user->email = $email;
            $user->save();

            // store in session
            session(['currentUser' => $user, 'user' => $instagram->getUser(), 'profilePicture' => $profilePicture]);

            // return view
            return view('sort.create', [
                'medias' => $medias,
                'hasNext' => $instagram->mediaHasNextPage,
                'maxId' => $instagram->mediaMaxId
            ]);
        } catch (ValidationException $e) {
            return redirect()
                ->back()
                ->withErrors(['message' => $e->validator->errors()->all()]);
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withErrors(['message' => $e->getMessage()]);
        }
    }

    public function show(Request $request, $id)
    {
        try {
            $sort = Sort::findOrFail($id);

            if ($sort->downloaded == 1) {
                return redirect()
                    ->route('sort.generateForm', ['id' => $id]);
            } else {
                $percent = (int)((100 * $sort->comments_downloaded) / $sort->comments_count);
            }

            $data = [
                'sort' => $sort,
                'media' => (object) $sort->media_data,
                'percent' => $percent
            ];

            return view('sort.show', $data);
        } catch (\Exception $e) {
            return redirect()
                ->to('/')
                ->withErrors(['message' => $e->getMessage()]);
        }

    }

    public function generate(Request $request, $id)
    {
        try {
            $sort = Sort::findOrFail($id);

            if ($sort->downloaded == 0) {
                throw new \Exception('Você não pode sortear um número, ainda estamos baixando os comentários.');
            }

            // validate request
            $request->validate([
                'numWinners' => 'required|integer'
            ]);

            $dbHelper = new SqliteDatabaseHelper($sort->media_code);
            $sqlCommands = [];

            // apply filters
            if ($request->input('keywordFilter')) {
                $st = $dbHelper->db->prepare('DELETE FROM comments WHERE comment_text NOT LIKE :query');
                $st->bindValue(':query', '%' . $request->input('keywordFilter') . '%');
                $sqlCommands[] = $st;
            }

            if ($request->input('uniqueUserComment')) {
                $st = $dbHelper->db->prepare('DELETE FROM comments WHERE rowid NOT IN (SELECT min(rowid) FROM comments GROUP BY user)');

                $sqlCommands[] = $st;
            }

            if ($request->input('uniqueComments')) {
                $st = $dbHelper->db->prepare('DELETE FROM comments WHERE rowid NOT IN (SELECT min(rowid) FROM comments GROUP BY user, comment_text)');
                $sqlCommands[] = $st;
            }

            $begin = false;
            if (count($sqlCommands) > 0) {
                $begin = true;
                $dbHelper->db->exec('BEGIN;');

                foreach ($sqlCommands as $command) {
                    $command->execute();
                }
            }

            $numWinners = $request->input('numWinners');
            $uniqueWinners = $request->input('uniqueWinners');

            if ($uniqueWinners) {
                $sql = 'SELECT MIN(rowid) as rowid, * FROM comments WHERE rowid IN (SELECT rowid FROM comments ORDER BY RANDOM() LIMIT ' . $numWinners . ') GROUP BY user';
            } else {
                $sql = 'SELECT rowid, * FROM comments WHERE rowid IN (SELECT rowid FROM comments ORDER BY RANDOM() LIMIT ' . $numWinners . ')';
            }

            $sorted = $dbHelper->db->prepare($sql);
            $sorted->execute();
            $result = $sorted->fetchAll(PDO::FETCH_CLASS);

            if ($begin)
                $dbHelper->db->exec('ROLLBACK;');

            return response()
                ->json(['data' => $result]);
        } catch (ValidationException $e) {
            return response()
                ->json(['error' => true, 'message' => $e->validator->errors()->all()], \App\Http\API\Controllers\BaseController::HTTP_STATUS_BAD);
        } catch (\Exception $e) {
            return response()
                ->json(['error' => true, 'message' => $e->getMessage()], \App\Http\API\Controllers\BaseController::HTTP_STATUS_BAD);
        }
    }

    public function updateNotify(Request $request, $id)
    {
        try {
            $playerId = $request->input('playerId');
            $sort = Sort::findOrFail($id);

            if ($sort->downloaded) {
                throw new \Exception('Os comentários já foram concluídos.');
            }

            $sort->notify_data = is_null($playerId) ? null : ['platform' => 'web', 'id' => $playerId];
            $sort->save();

            return null;
        } catch (\Exception $e) {
            return response()
                ->json(['error' => true, 'message' => $e->getMessage()], \App\Http\API\Controllers\BaseController::HTTP_STATUS_BAD);
        }
    }

    public function generateForm(Request $request, $id)
    {
        try {
            $sort = Sort::findOrFail($id);

            if ($sort->downloaded == 0) {
                return redirect()
                    ->route('sort.show', ['id' => $id]);
            }

            $data = [
                'sort' => $sort,
                'media' => new Media($sort->media_data)
            ];

            return view('sort.generate', $data);
        } catch (\Exception $e) {
            return redirect()
                ->to('/')
                ->withErrors(['message' => $e->getMessage()]);
        }
    }

    public function status(Request $request, $id)
    {
        try {
            $sort = Sort::findOrFail($id);

            if ($sort->downloaded) {
                return response()
                    ->json(['downloaded' => true]);
            }

            $percent = (int) ((100*$sort->comments_downloaded)/$sort->comments_count);

            return response()
                ->json(['downloaded' => false, 'percent' => $percent]);
        } catch (\Exception $e) {
            return response()
                ->json(['error' => true, 'message' => $e->getMessage()], \App\Http\API\Controllers\BaseController::HTTP_STATUS_BAD);
        }
    }

    public function store(Request $request)
    {
        try {
            $code = $request->input('code');

            // validate request
            $request->validate([
                'code' => 'required'
            ]);

            // get data from instagram
            $mediaObject = Instagram::getMediaByCode($code);
            $currentUser = session('currentUser');
            $mediaData = new Media($mediaObject);

            // create in database
            $sort = new Sort;
            $sort->requested_user_id = $currentUser->id;
            $sort->comments_count = $mediaData->commentsCount;
            $sort->media_code = $code;
            $sort->media_data = $mediaData;
            $sort->comments_downloaded = 0;
            $sort->downloaded = 0;
            $sort->save();

            // start job
            dispatch(new DownloadComments($sort->id, $code))
                ->onConnection('async')
                ->onQueue('downloadComments');

            return redirect()
                ->route('sort.show', ['id' => $sort->id]);
        } catch (ValidationException $e) {
            return redirect()
                ->back()
                ->withErrors(['message' => $e->validator->errors()->all()]);
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->withErrors(['message' => $e->getMessage()]);
        }
    }

}