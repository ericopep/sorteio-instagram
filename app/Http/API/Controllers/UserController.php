<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 04/10/18
 * Time: 10:56
 */

namespace App\Http\API\Controllers;

use App\Backend\Instagram\Instagram;
use App\Backend\Instagram\Model\Media;
use App\Helpers\InstagramHelper;
use Illuminate\Http\Request;

class UserController extends BaseController
{

    public function getProfilePicture(Request $request)
    {
        try {
            $username = $request->input('username');

            if (is_null($username))
                throw new \Exception('Digite algum usuário');

            $instagram = new Instagram($username);
            return $this->json(['url' => $instagram->getUserProfilePicture()]);
        } catch (\Exception $e) {
            return $this->json($e->getMessage(), true);
        }
    }

    public function getMedia(Request $request)
    {
        try {
            $maxId = $request->input('maxId');
            $username = $request->input('username');
            $instagram = new Instagram($username);
            $medias = $instagram->getMedias($maxId);

            return $this->json([
                'media' => $medias,
                'hasNext' => $instagram->mediaHasNextPage,
                'maxId' => $instagram->mediaMaxId
            ]);
        } catch (\Exception $e) {
            return $this->json($e->getMessage(), true);
        }
    }

}