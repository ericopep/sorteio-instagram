<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 04/10/18
 * Time: 10:56
 */

namespace App\Http\API\Controllers;

use App\Backend\Instagram\Instagram;
use App\Backend\Instagram\Model\Media;
use App\Helpers\InstagramHelper;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class MediaController extends BaseController
{
    const INSTAGRAM_MEDIA_URL_PATTERN = '/(https?:\/\/(www\.)?)?instagram\.com(\/p\/\w+\/?)/';

    public function search(Request $request)
    {
        try {
            // validate request
            $request->validate([
                'q' => 'required'
            ]);

            $q = $request->input('q');

            if (!preg_match(self::INSTAGRAM_MEDIA_URL_PATTERN, $q)) {
                throw new \Exception('A url não é válida');
            }

            preg_match_all(self::INSTAGRAM_MEDIA_URL_PATTERN, $q, $matches);

            $code = $matches[3][0];
            $code = substr($code, 3);
            $code = rtrim($code, '/');

            $mediaObject = Instagram::getMediaByCode($code);
            $media = new Media($mediaObject);

            return $this->json(['media' => $media]);
        } catch (ValidationException $e) {
            return $this->json($e->validator->errors()->all(), true);
        } catch (\Exception $e) {
            return $this->json($e->getMessage(), true);
        }
    }

}