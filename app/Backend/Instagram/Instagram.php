<?php

namespace App\Backend\Instagram;

use App\Backend\Exceptions\PrivateUserException;
use App\Backend\Instagram\Model\Media;

class Instagram
{
    private $username = null;
    private $user = null;
    private $scraper = null;

    /*
     * Media pages attributes
     */
    public $mediaHasNextPage = null;
    public $mediaMaxId = null;

    /**
     * Instagram constructor.
     * @param $username
     * @throws PrivateUserException
     * @throws \InstagramScraper\Exception\InstagramException
     * @throws \InstagramScraper\Exception\InstagramNotFoundException
     */
    public function __construct($username = null)
    {
        $this->scraper = self::createScraper();

        if (is_null($username))
            return;

        $this->username = $username;
        $this->user = $this->scraper->getAccount($this->username);

        if ($this->user->isPrivate())
            throw new PrivateUserException('O usuário tem o perfil privado!');
    }

    public static function createScraper()
    {
        return new InstagramScraper();
    }

    /**
     * @return \InstagramScraper\Model\Account|null
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getMedias($maxId = null)
    {
        $medias = $this->scraper->getPaginateMedias($this->username, $maxId);
        $this->mediaHasNextPage = $medias['hasNextPage'];

        if ($this->mediaHasNextPage)
            $this->mediaMaxId = $medias['maxId'];

        $medias = $medias['medias'];
        $mediasList = [];

        foreach ($medias as $media) {
            $mediasList[] = new Media($media);
        }

        return $mediasList;
    }

    public function getMediasNextPage()
    {
        if ($this->mediaHasNextPage)
            return $this->getMedias($this->mediaMaxId);

        return null;
    }

    public static function getMediaComments($code, $maxId = null)
    {
        $scraper = self::createScraper();

        return $scraper->getMediaCommentsByCode($code, InstagramScraper::MAX_COMMENTS_PER_REQUEST, $maxId);
    }

    public function getUserProfilePicture()
    {
        return $this->user->getProfilePicUrl();
    }

    public static function getMediaByCode($code)
    {
        $scraper = self::createScraper();

        return $scraper->getMediaByCode($code);
    }

}
