<?php

namespace App\Backend\Instagram\Model;

use InstagramScraper\Model\Account;

class User extends BaseModel
{
    public $username;
    public $profilePicUrl;

    public function __construct($data)
    {
        if ($data instanceof Account) {
            $this->username = $data->getUsername();
            $this->profilePicUrl = $data->getProfilePicUrl();
        } else if (is_array($data)) {
            $this->set_object_vars($this, $data);
        }
    }
}

