<?php

namespace App\Console;

use App\Console\Commands\CFUpdater;
use App\Console\Commands\GetIpAddress;
use App\Models\Sort;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CFUpdater::class,
        GetIpAddress::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $date = Carbon::now()->subHours(3);
            $sorts = Sort::where('created_at', '<=', $date)->get();

            foreach ($sorts as $item) {
                Storage::disk('local')->delete('db/' . $item->media_code . '.sqlite');
            }
        })->daily();

        $schedule->call(function() {
            Artisan::call('cf:update');
        })->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
