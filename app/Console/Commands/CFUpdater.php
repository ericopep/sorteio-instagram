<?php

namespace App\Console\Commands;

use App\Context\CFUpdate;
use App\Context\IpAddress;
use Illuminate\Console\Command;

/**
 * Class CFUpdate
 * @package App\Console\Commands
 */
class CFUpdater extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cf:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualizar o ip no cloudflare';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $yourIp = app(IpAddress::class)->handle();

        $email = env('CLOUDFLARE_EMAIL');
        $key = env('CLOUDFLARE_API_KEY');
        $domain = env('CLOUDFLARE_DOMAIN');
        $only_tld = $domain;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.cloudflare.com/client/v4/zones?name=$only_tld&status=active&page=1&per_page=20&order=status&direction=desc&match=all");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $headers = array();
        $headers[] = "X-Auth-Email: {$email}";
        $headers[] = "X-Auth-Key: {$key}";
        $headers[] = "Content-Type: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $this->error('Error: ' . curl_error($ch));
        }
        $zone_id = json_decode($result)->result[0]->id;
        curl_setopt($ch, CURLOPT_URL, "https://api.cloudflare.com/client/v4/zones/$zone_id/dns_records?&name={$domain}&page=1&per_page=20&order=!type&direction=desc&match=all");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $headers = array();
        $headers[] = "X-Auth-Email: {$email}";
        $headers[] = "X-Auth-Key: {$key}";
        $headers[] = "Content-Type: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $this->error('Error: ' . curl_error($ch));
        }
        $domain_id = json_decode($result)->result[0]->id;
        $record_type = json_decode($result)->result[0]->type;
        curl_setopt($ch, CURLOPT_URL, "https://api.cloudflare.com/client/v4/zones/$zone_id/dns_records/$domain_id");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"id\":\"$domain_id\",\"type\":\"$record_type\",\"name\":\"{$domain}\",\"content\":\"{$yourIp}\",\"data\":{}}");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        $headers = array();
        $headers[] = "X-Auth-Email: {$email}";
        $headers[] = "X-Auth-Key: {key}";
        $headers[] = "Content-Type: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $this->error('Error: ' . curl_error($ch));
        }
        curl_close ($ch);

        $this->info('atualizou');
    }
}
