<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => '\App\Http\API\Controllers'], function () {

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });


    /**
     * ALl routes to a instagram request
     */
    Route::group(['prefix' => 'social'], function() {

        Route::group(['prefix' => 'media'], function() {

            Route::get('search', 'MediaController@search');


        });


        Route::group(['prefix' => 'user'], function() {

            Route::get('profile-picture', 'UserController@getProfilePicture');
            Route::get('medias', 'UserController@getMedia');


        });

    });

});
